// Step 3
db.simpleRoom.insertOne({
        "name": "Single",
        "accomodates": 2,
        "price": 1000,
        "description": "A simple room with all the basic necessities",
        "rooms_available": 10,
        "isAvailable": false
    
});


// Step 4
db.additionalRooms.insertMany([{
    "name": "double",
    "accomodates": 3,
    "price": 2000,
    "description": "A room fit for a small family going on a vacation",
    "rooms_available": 5,
    "isAvailable": false
    },
    {
    "name": "double",
    "accomodates": 3,
    "price": 2000,
    "description": "A room with a queen sized bed perfect for a simple getaway",
    "rooms_available": 15,
    "isAvailable": false
    }]);

// Step 5
db.additionalRooms.find({"name": "double"});

// Step 6
db.additionalRooms.updateOne({"_id" : ObjectId("61763c81dd1b8c162017d5b3")},{
$set: {"rooms_available": 0}
});

// Step 7
db.additionalRooms.deleteMany({"rooms_available": 0});